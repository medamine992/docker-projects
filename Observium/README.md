# Docker container for Observium Community Edition
Observium is network monitoring with intuition. It is a low-maintenance auto-discovering network monitoring platform supporting a wide range of device types, platforms and operating systems including Cisco, Windows, Linux, HP, Juniper, Dell, FreeBSD, Brocade, Netscaler, NetApp and many more. Observium focuses on providing a beautiful and powerful yet simple and intuitive interface to the health and status of your network. For more information, go to http://www.observium.org site.


### A. Manual Run Containers
- Prepare working directory for docker containers, for example below.
```
  $ mkdir -p /home/docker/observium/{data,logs,rrd}
```
- Run official MariaDB container
```
  $ docker run --name observiumdb \
    -v /home/docker/observium/data:/var/lib/mysql \
    -e MYSQL_ROOT_PASSWORD=passw0rd \
    -e MYSQL_USER=observium \
    -e MYSQL_PASSWORD=passw0rd \
    -e MYSQL_DATABASE=observium \
    -e TZ=$(cat /etc/timezone) \
    mariadb
```

- Run this Observium container
```
  $ docker run --name observiumapp --link observiumdb:observiumdb \
    -v /home/docker/observium/logs:/opt/observium/logs \
    -v /home/docker/observium/rrd:/opt/observium/rrd \
    -e OBSERVIUM_ADMIN_USER=admin \
    -e OBSERVIUM_ADMIN_PASS=passw0rd \
    -e OBSERVIUM_DB_HOST=observiumdb \
    -e OBSERVIUM_DB_USER=observium \
    -e OBSERVIUM_DB_PASS=passw0rd \
    -e OBSERVIUM_DB_NAME=observium \
    -e OBSERVIUM_BASE_URL=http://yourserver.yourdomain:8080 \
    -e TZ=Asia/Bangkok
    -p 8080:80
    mbixtech/observium
```
